# API Docs

## I. Members API

### I.1. Create member (Register member)

| Description  | Endpoint | Method | Is Private? | Require Realtime Response |
| --- | --- | --- | --- | --- |
| URL  | /api/v2/members | HTTP/POST (restful)|No|Yes|

#### Tham số đầu vào

Request Header:

| No  | Tên | Bắt buộc | Diễn giải | Kiểu | Min | Max |
| --- | --- | --- | --- | --- | --- | --- |
| 1 | x-client-id | (tick) | Định danh của đầu mối tích hợp | String | 1 | 20 |

Request Body:

| No  | Tên | Bắt buộc | Diễn giải | Kiểu | Min | Max |
| --- | --- | --- | --- | --- | --- | --- |
| 1 | external_id | (tick) | Mã khách hàng tham chiếu tại hệ thống đối tác | String | 1 | 50 |
| 2 | name | (tick) | Tên khách hàng | String | 3 | 100 |
| 3 | phone | (tick) | Phone theo định dạng SĐT của VN (Không có +84) | String | 0 | 15 |
| 4 | email | (error) | Email format | String | 0 | 50 |
| 5 | gender | (error) | Giới tính: F - Female, M - Male, N - Undefined | String | 0 | 1 |
| 6 | id_card_number | (error) | Số CMND | String | 9 | 12 |
| 7 | passport_number | (error) | Số Passport | String | 8 | 12 |
| 8 | id_citizen_number | (error) | Số thẻ căn cước | String | 12 | 12 |
| 9 | address | (error) | Địa chỉ | String | 0 | 200 |
| 10 | date_of_birth | (error) | Ngày sinh (yyyyMMdd) | String | 0 | 8 |
| 11 | checksum | (tick) | Checksum Format = sha256(requestId + "&#124;" + clientId + "&#124;" + externalId + "&#124;" + phone + "&#124;" + email + "&#124;" + gender + "&#124;" + idCardNumber + "&#124;" + passportNumber + "&#124;" + idCitizenNumber + "&#124;" + dateOfBirth + "&#124;" + secretKey) | String | 64 | 64 |
| 12 | request_id | (tick) | Mã Request tại merchant | String | 5 | 36 |

#### Kết quả trả về

| No  | Tên | Mô tả | Kiểu | Mặc định | Ví dụ |
| --- | --- | --- | --- | --- | --- |
| 1 | id | Định danh của đầu mối tích hợp | String |   |  |

Ví dụ:
```
{
  "code": "00",
  "result": {
    "id": 21
  },
  "message": "Successful",
  "extra": {
    "response_id": "XXX",
    "checksum": "YYYY"
  }
}
```

#### Error message

| Error code  | Type | Error message |
| --- | --- | --- |
| 00 | String | Successful |
| 01 | String | Invalid network |
| 04 | String | Invalid Customer name length |
| 05 | String | Invalid phone number format |
| 06 | String | Invalid email format |
| 07 | String | Invalid gender |
| 08 | String | Invalid ID Card number |
| 09 | String | Invalid Passport Number |
| 10 | String | Invalid ID Citizen number |
| 11 | String | Invalid address length |
| 12 | String | Invalid date of birth |
| 18 | String | Invalid requestId | 
| 99 | String | Internal errors |
| 100 | String | Not found Client in the system |
| 111 | String | Existed ExternalId |
| 117 | String | Not found terminal in the system |
| 200 | String | Invalid checksum |
| 201 | String | Failed to register member |

